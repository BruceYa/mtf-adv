# -*- coding: utf-8 -*-
"""
Created on Tue Feb 25 08:51:17 2020

@author: Trader
"""
#This file will use the excel file which read data from BB to analysis and to push data to DB

import pandas as pd
from sqlalchemy import create_engine
import tkinter as tk
import sys
from datetime import datetime

class Analysis:
    """Class for all analysis operation""" 
    def __init__(self, file_address):
        """Init from file address"""
        self.file_address = file_address

    def get_MTF_universe(self, MTF_universe):
        """Get file from IO"""
        self.MTF_universe = MTF_universe
        
    def analysis_bundle(self):
        """analysis data"""
        def abnormal_value(self):
            """abnormal data filter"""
            self.MTF_universe[(self.MTF_universe['diff']<=0.5)
            |(self.MTF_universe['diff']>=1.5)].to_excel(self.file_address+'\\MTF-BB-abnormal.xlsx',index=False)
        
        def zero_adv(self):
            """ticker with zero adv"""
            self.MTF_universe[self.MTF_universe['new_adv']==0.0].to_excel(self.file_address+'\\MTF-BB-zero-adv.xlsx',index=False)
        
        def visualization(self):
            """TBD"""
            pass
                
        abnormal_value(self)
        zero_adv(self)
        visualization(self)

class IO:
    """Class for all I/O operation"""    
    def __init__(self, file_address, DB_address):
        """Init from file address and DB address"""
        self.file_address = file_address
        self.DB_address = DB_address
        
    def read_file(self):
        """Read file with error handling"""
        try:
            self.MTF_universe = pd.read_excel(self.file_address+'\\MTF-BB.xlsx')
            print('MTF_universe successfully loaded from '+self.file_address+'\\MTF-BB.xlsx')
            return self.MTF_universe 
        except FileNotFoundError as e:
            print(e)
    
    def connect(self):
        """Connect to server with error handling"""
        try:
            self.engine = create_engine(self.DB_address)
            self.connection = self.engine.connect()
            print('Connection successfully setablished with '+self.DB_address)
        except BaseException as e:
            print (e)
    
    def push_to_DB(self):
        """Push data to a table named universe_temp"""
        MTF_universe_NEW=self.MTF_universe[['ticker', 'primaryTicker', 'exchange', 'new_adv', 'modified']]
        MTF_universe_NEW.columns=['ticker', 'primaryTicker', 'exchange', 'adv', 'modified']
        MTF_universe_NEW['modified']=[datetime.utcnow().replace(microsecond=0)]*len(MTF_universe_NEW)
        MTF_universe_NEW['adv'].fillna(0,inplace=True)
        MTF_universe_NEW.to_sql('universe_temp', self.connection,index=False, if_exists='replace')

class UI:
    """Class for the UI"""  
    def __init__(self):
        """Init the GUI"""
        file_address = 'C:\\Users\\Trader\\Desktop\\'
#        DB_address = 'mysql+pymysql://orders:pentalax@10.0.1.20/qtt' 
        DB_address = 'mysql+pymysql://root:rhou5dnhQded@localhost:3306/qtt' 
        self.window = tk.Tk()
        self.window.geometry("650x500")
        self.window.title('Bloomberg data to DB')
        self.IO_Operator = IO(file_address, DB_address)
        self.Analysis_Operator = Analysis(file_address)

        self.fat_finger=False
        # This boolean varible is to prevent we push data back to DB without analysis
        # False-need to do analysis 
        # True-analysis is done
        
        self.window.protocol('WM_DELETE_WINDOW', self.close_app)
        
        ############################################### Title, indentation and empty columns        
        tk.Label(self.window, text = 'MTF ADV Push Back', font = 30).grid(row = 0, column = 0, columnspan = 9)
        tk.Label(self.window, text = '  ', font = 30).grid(row = 1, column = 0, rowspan = 2)
        tk.Label(self.window, text = '   ', font = 30).grid(row = 1, column = 6, rowspan = 2)
        for i in[2,4,6,8]:
            tk.Label(self.window, text = ' ', font = 10).grid(row = i, column=0, columnspan = 9)
        
        ############################################### DB address inpou 
        tk.Label(self.window, text = 'DB address: ', font = 30).grid(row = 1, column = 1)
        default_DB_address = tk.StringVar()
        default_DB_address.set(DB_address)
        self.DB_address_input = tk.Entry(self.window, textvariable = default_DB_address, width = 40, font = 30)
        self.DB_address_input.grid(row = 1, column = 2, columnspan = 3)
        
        ############################################### Connect to DB button
        tk.Button(self.window, text = "Connect", command = self.connect, height = 1, width = 15, font = 30).grid(row = 1, column = 7, columnspan = 2)
        
        ############################################### file address inpou  
        tk.Label(self.window, text = 'File address: ', font = 30).grid(row = 3, column = 1)
        default_file_address = tk.StringVar()
        default_file_address.set(file_address)
        self.file_address_input = tk.Entry(self.window, textvariable=default_file_address, width = 40, font=30)
        self.file_address_input.grid(row=3, column=2, columnspan=3)
        
        ############################################### Read file button   
        tk.Button(self.window, text = "Read file", command = self.read_file, height = 1, width = 15, font = 30).grid(row = 3, column = 7, columnspan = 3)
        
        ############################################### Analysis button   
        tk.Button(self.window, text = "Analysis", command = self.analysis, height = 1, width = 30, font = 30).grid(row = 5, column = 0, columnspan = 9)

        ############################################### Push to DB file button   
        tk.Button(self.window, text = "Push to DB", command = self.push_to_DB, height = 1, width = 30, font = 30).grid(row = 7, column = 0, columnspan = 9)
            
        ############################################### Text box to display error message  
        self.text_box = tk.Text(self.window, wrap = 'word', height = 12, width = 50, font = 30)
        self.text_box.grid(row = 9, column = 0, columnspan = 9)
        sys.stdout = self.StdoutRedirector(self.text_box)
            
        tk.mainloop()
        
    def close_app(self):
        """close message box"""
        if tk.messagebox.askokcancel("Close", "Exit the Window"):
            self.window.destroy()
            
    class StdoutRedirector(object):
        """For redirecting the stdout to text box"""
        def __init__(self,text_widget):
            self.text_space = text_widget
    
        def write(self,string):
            self.text_space.insert('end', string)
            self.text_space.see('end')
            
        def flush(self):
            pass
        
    def connect(self):
        """connect function in UI"""
        self.IO_Operator.DB_address = self.DB_address_input.get()
        self.IO_Operator.connect()
    
    def read_file(self):
        """read file function in UI"""
        self.IO_Operator.file_address = self.file_address_input.get()
        self.IO_Operator.read_file()
    
    def analysis(self):
        """analysis function in UI, we want to make sure we read file first an then try to analysis"""
        self.Analysis_Operator.file_address = self.file_address_input.get()
        if hasattr(self.IO_Operator, 'MTF_universe'):
            # Only after call the read_file function will the global MTF_universe exist
            print('Start analysis, please wait')
            self.Analysis_Operator.get_MTF_universe(self.IO_Operator.MTF_universe)
            self.Analysis_Operator.analysis_bundle()
            self.fat_finger = True
            # The analysis is done so we can push data back to DB
            print('Analysis completed, please find the result')
        else:
            print('Please read file first')
            pass
    
    def push_to_DB(self):
        """push to DB function in UI"""
        if self.fat_finger:# See if the analysis is done
            print('Pushing')
            self.IO_Operator.push_to_DB()
            print('Pushing Succeed')
        else:
            print('Please analysis first then push the data to DB')
            pass
    
if __name__ == "__main__":
    UI()  
