# -*- coding: utf-8 -*-
"""
Created on Fri Feb 21 14:23:58 2020

@author: Trader
"""
#this file will generate an excel file to read data from BB by using tickers from our DB
import pandas as pd
from sqlalchemy import create_engine

#Setting up connection
engine = create_engine("mysql+pymysql://orders:pentalax@10.0.1.20/qtt")
connection = engine.connect()

#Listing all available products/Exchanges
query = 'select ticker, primaryTicker, exchange, modified, adv from qtt.universe;'
universe = pd.read_sql(query, connection)

#Ticker in Amsterdam
NA_universe=universe[(universe.ticker.str.endswith('NA Equity'))
            |(universe.primaryTicker.str.endswith('NA Equity'))]#NA for Amsterdam

#EB for BATS, QX for Aquis, TQ for Turquoise, IX for Chi-X
MTF_universe=NA_universe[(universe.ticker.str.endswith('TQ Equity'))
            |(universe.ticker.str.endswith('NA Equity'))
            |(universe.ticker.str.endswith('QX Equity'))
            |(universe.ticker.str.endswith('EB Equity'))
            |(universe.ticker.str.endswith('IX Equity'))]

#To see ticker didn't pass the filter
#NA_universe[~NA_universe.index.isin(MTF_universe.index)]

#Excel modification
MTF_universe.columns=['ticker', 'primaryTicker', 'exchange', 'modified', 'old_adv']
MTF_universe['new_adv']=['=BDP("'+x+'","VOLUME_AVG_20D")' for x in MTF_universe.ticker]
MTF_universe['diff']=['=F'+str(i)+'/E'+str(i) for i in range(2,len(MTF_universe)+2)]

#Output
MTF_universe.to_excel('C:\\Users\\Trader\\Desktop\\MTF-BB.xlsx',index=False)

